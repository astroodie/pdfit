# README #

Fit the parametrize deleptonization parameters that described in Liebendorfer 2005. 

### Usage ###


1. in python

```
yeprofile('bounce_data.d',rho1,rho2,yc,yh)

```

2. in shell
```
python parameterfit.py fname d1 d2 yc yh
```

for example,

```
python parameterfit.py grw20_bounce.d 4.e8 7.e12 0.275 0.015
```

**See the Wiki page for example fitted parameters.**