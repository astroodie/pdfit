import numpy as np
import matplotlib.pyplot as plt

#-----------------------------------------------------------
#
# Rewrite the Parametrize Deleptonization (PD) fitting code in python
#
# Kuo-Chuan Pan 2016.04.08
#

def getprofile(fname,npoints,unpack=True):
    """
    Load Agile-Boltztrann's bounce data

    fname: file name
    npoints: [102] number of data points in agile-boltztrann (usaully=102)
    unpack: [True] 
    """
    SKIP_ROWS = 30
    END_ROWS  = SKIP_ROWS + npoints 

    db = []
    f = open(fname,'r')
    iline = 0
    for line in f:
        iline += 1
        if (iline > SKIP_ROWS) and (iline <= END_ROWS):
            db.append(line.split())
    if unpack:
        out = []
        ncol = len(db[0])
        for n in xrange(ncol):
            out.append(np.array([]))
        for line in db:
            for n,s in enumerate(line):
                try:
                    v = float(s)
                except:
                    v = s
                out[n] = np.append(out[n],v)
        db = out
    return db

def yeprofile(data_bounce,d1,d2,yc,yh):

    """
    Fit Ye profile 

    d1= rho1 
    d2= rho2
    yc= central ye
    yh= ye hump
    """
    
    npoints = 102  # nmber of points in Agile-Boltztran

    db = getprofile(data_bounce,npoints)
    db[1] = db[1]*1.e6  # change radius units to cm
    
    dens = db[3]
    ye   = db[5]

    rho1 = np.log10(d1)
    rho2 = np.log10(d2)
    ld   = np.log10(dens)
    rhoh = 0.5*(rho1+rho2)

    x = np.ones(len(dens))
    yfit = np.ones(len(dens))
    yres = np.ones(len(dens))
    yhump = np.ones(len(dens))
    for i in xrange(npoints):
        x[i] = max(-1,min(1.0,2.0*(ld[i]-rhoh)/(rho2-rho1)))
        yfit[i] = 0.5*(yc+0.5) + 0.5*(yc-0.5)*x[i]

    yres = ye - yfit
    for i in xrange(npoints):
        x[i] = np.abs(x[i])
        yhump[i] = yh*(1.0-x[i]+4.0*x[i]*(x[i]-0.5)*(x[i]-1.0))

    yfit = yfit + yhump

    # plot resuts
    plt.figure(1)
    plt.text(1e6,0.4,'rho1='+str(d1))
    plt.text(1e6,0.35,'rho2='+str(d2))
    plt.text(1e6,0.3 ,'yc  ='+str(yc))
    plt.text(1e6,0.25 ,'yh  ='+str(yh))
    plt.plot(dens,ye,'k-',label=r"$Y_e$")
    plt.plot(dens,yfit,'r-',label=r"$Y_{fit}$")
    plt.plot([1e3,1e15],[0.05,0.05],color='0.5')
    plt.plot([1e3,1e15],[-0.05,-0.05],color='0.5')
    plt.plot(dens,yres,'g-',label=r"$Y_{res}$")
    plt.plot(dens,yhump,'b-',label=r"$Y_{hump}$")
    plt.xlabel('Density (cgs)')
    plt.ylabel('Electron Fraction')
    plt.legend(loc='upper right')
    plt.ylim([-0.1,0.55])
    plt.xlim([1e5,1e15])
    plt.xscale('log')
    plt.title(data_bounce)
    plt.savefig('fig_pdfit.png')
    return

if __name__=='__main__':

    import sys
    narg = len(sys.argv)
    if narg==1:
        # no arguments, run the example
        db = 'grw20_bounce.d'
        d1 = 4.0e8
        d2 = 7.0e12
        yc = 0.275
        yh = 0.015
    elif narg==6:
        db = str(sys.argv[1])
        d1 = float(sys.argv[2])
        d2 = float(sys.argv[3])
        yc = float(sys.argv[4])
        yh = float(sys.argv[5])
    else:
        print "Error: wrong arguments."
        print "Usage: python parametefit.py fname d1 d2 yc yh"
        print " "
        print "Example: python parameterfit.py grw20_bounce.d 4.e8 7.e12 0.275 0.015"
        quit()

    #--- example --- 
    # s20 WH 2007 GR 
    yeprofile(db,d1,d2,yc,yh)


